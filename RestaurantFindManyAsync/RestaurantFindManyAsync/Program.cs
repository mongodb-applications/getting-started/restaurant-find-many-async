﻿using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace RestaurantFindManyAsync;

public class FindManyAsync
{
    private static IMongoCollection<Restaurant>? restaurantsCollection;
    private const string MongoConnectionString = "mongodb+srv://spravce:JYHoYzXaRFMvZah5@learn-mongodb-cluster.lkbhbsw.mongodb.net/?retryWrites=true&w=majority";

    public static async Task Main(string[] args)
    {
        Setup();

        // Find multiple documents using builders
        Console.WriteLine("Finding documents with builders...:");
        List<Restaurant> restaurantsBuilders = await FindMultipleRestaurantsBuilderAsync();
        Console.WriteLine($"Number of documents found: {restaurantsBuilders.Count}");

        // Extra space for console readability 
        Console.WriteLine();

        // Find multiple documents using LINQ
        Console.WriteLine("Finding documents with LINQ...:");
        List<Restaurant> restaurantsLinq = await FindMultipleRestaurantsLinqAsync();
        Console.WriteLine($"Number of documents found: {restaurantsLinq.Count}");

        Console.WriteLine();

        // Find all documents
        Console.WriteLine("Finding all documents builders...:");
        List<Restaurant> allRestaurantsBuilder = await FindAllRestaurantsBuilderAsync();
        Console.WriteLine($"Number of documents found: {allRestaurantsBuilder.Count}");
    
        Console.WriteLine();

        // Find all documents
        Console.WriteLine("Finding all documents LINQ...:");
        List<Restaurant> allRestaurantsLINQ = await FindAllRestaurantsLinqAsync();
        Console.WriteLine($"Number of documents found: {allRestaurantsLINQ.Count}");
  
    }

    private static async Task<List<Restaurant>> FindMultipleRestaurantsBuilderAsync()
    {
        // start-find-builders-async
        FilterDefinition<Restaurant> filter = Builders<Restaurant>.Filter
            .Eq(r => r.Cuisine, "Pizza");

        return await restaurantsCollection.Find(filter).ToListAsync();
        // end-find-builders-async
    }

    private static async Task<List<Restaurant>> FindMultipleRestaurantsLinqAsync()
    {
        // start-find-linq-async
        return await restaurantsCollection.AsQueryable()
            .Where(r => r.Cuisine == "Pizza").ToListAsync();
        // end-find-linq-async
    }

    private static async Task<List<Restaurant>> FindAllRestaurantsBuilderAsync()
    {
        // start-find-all-async
        FilterDefinition<Restaurant> filter = Builders<Restaurant>.Filter.Empty;

        return await restaurantsCollection.Find(filter)
            .ToListAsync();
        // end-find-all-async
    }

    private static async Task<List<Restaurant>> FindAllRestaurantsLinqAsync()
    {
        // start-find-linq-async
        return await restaurantsCollection.AsQueryable().ToListAsync();
        // end-find-linq-async
    }

    private static void Setup()
    {
        // This allows automapping of the camelCase database fields to our models. 
        ConventionPack camelCaseConvention = new ConventionPack { new CamelCaseElementNameConvention() };
        ConventionRegistry.Register("CamelCase", camelCaseConvention, type => true);

        // Establish the connection to MongoDB and get the restaurants database
        IMongoClient mongoClient = new MongoClient(MongoConnectionString);
        IMongoDatabase restaurantsDatabase = mongoClient.GetDatabase("sample_restaurants");
        restaurantsCollection = restaurantsDatabase.GetCollection<Restaurant>("restaurants");
    }
}
